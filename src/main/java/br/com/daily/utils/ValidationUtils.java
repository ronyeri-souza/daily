package br.com.daily.utils;

import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.daily.exceptions.InvalidPasswordException;
import br.com.daily.model.enums.ErrorMessagesEnum;

public class ValidationUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationUtils.class);

	public static ValidationUtils getInstance() {
		return new ValidationUtils();
	}

	public void validatePasswordStrength(String password) throws InvalidPasswordException {
		LOGGER.debug("Validating that the password matches the regex...");
		Pattern pattern = Pattern.compile("^(?=.*[A-Z])(?=.*[!#@$%&])(?=.*[0-9])(?=.*[a-z]).{8,}$");
		if (!pattern.matcher(password).matches()) {
			throw new InvalidPasswordException(ErrorMessagesEnum.INVALID_PASSWORD_REGEX.getError());
		}
	}
}
