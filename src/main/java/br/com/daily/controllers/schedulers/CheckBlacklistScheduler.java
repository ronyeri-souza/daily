package br.com.daily.controllers.schedulers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.daily.service.BlacklistService;

@Component
@EnableAsync
public class CheckBlacklistScheduler {
	private static final Logger LOGGER = LoggerFactory.getLogger(CheckBlacklistScheduler.class);

	private BlacklistService blacklistService;

	public CheckBlacklistScheduler(BlacklistService blacklistService) {
		this.blacklistService = blacklistService;
	}

	@Async
	@Scheduled(fixedDelay = 180000)
	public void clearUserSessionExpired() {
		LOGGER.info("Fetching and terminating expired blacklists...");
		this.blacklistService.clearExpiredBlacklists();
	}

}
