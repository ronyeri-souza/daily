package br.com.daily.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.daily.model.transport.ProjectDTO;
import br.com.daily.model.transport.PublicationDTO;
import br.com.daily.model.transport.UserDTO;
import br.com.daily.service.ProjectService;
import br.com.daily.service.PublicationService;
import br.com.daily.service.UserService;

@RestController
@RequestMapping("/project")
public class ProjectController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

	private ProjectService projectService;

	private UserService userService;

	private PublicationService publicationService;

	public ProjectController(ProjectService projectService, UserService userService,
			PublicationService publicationService) {
		this.projectService = projectService;
		this.userService = userService;
		this.publicationService = publicationService;
	}

	@GetMapping
	public ResponseEntity<List<ProjectDTO>> list(@RequestHeader("token") final String token) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			List<ProjectDTO> response = this.projectService.listProjects(userInSession);
			if (response.isEmpty()) {
				return ResponseEntity.noContent().build();
			}
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.noContent().build();
		}
	}

	@PostMapping
	public ResponseEntity<ProjectDTO> create(@RequestHeader("token") final String token,
			@RequestBody ProjectDTO projectDTO) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			ProjectDTO response = this.projectService.create(projectDTO, userInSession);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@DeleteMapping("/{projectUUID}")
	public ResponseEntity<Void> delete(@RequestHeader("token") final String token,
			@PathVariable("projectUUID") String projectUUID) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			this.projectService.delete(projectUUID, userInSession);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping("/{projectUUID}/access-token")
	public ResponseEntity<String> generateMemberAccessToken(@RequestHeader("token") final String token,
			@PathVariable("projectUUID") String projectUUID) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			String response = this.projectService.generateAccessMemberToken(projectUUID, userInSession);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping("/join/{accessToken}")
	public ResponseEntity<ProjectDTO> joinProject(@RequestHeader("token") final String token,
			@PathVariable("accessToken") String accessToken) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			ProjectDTO response = this.projectService.addMember(accessToken, userInSession);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping("{projectUUID}/leave/{userUUID}")
	public ResponseEntity<ProjectDTO> leaveProject(@RequestHeader("token") final String token,
			@PathVariable("projectUUID") String projectUUID, @PathVariable("userUUID") String userUUID)
			throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			this.projectService.deleteMember(userUUID, projectUUID, userInSession);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping("/{projectUUID}/publication")
	public ResponseEntity<PublicationDTO> createPublication(@RequestHeader("token") final String token,
			@RequestBody PublicationDTO publicationDTO, @PathVariable("projectUUID") String projectUUID)
			throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			PublicationDTO response = this.publicationService.create(publicationDTO, projectUUID, userInSession);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PutMapping("/{projectUUID}/publication/{publicationUUID}")
	public ResponseEntity<PublicationDTO> updatePublication(@RequestHeader("token") final String token,
			@RequestBody PublicationDTO publicationDTO, @PathVariable("publicationUUID") String publicationUUID)
			throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			PublicationDTO response = this.publicationService.update(publicationDTO, publicationUUID, userInSession);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@DeleteMapping("/{projectUUID}/publication/{publicationUUID}")
	public ResponseEntity<Void> deletePublication(@RequestHeader("token") final String token,
			@RequestBody PublicationDTO publicationDTO, @PathVariable("publicationUUID") String publicationUUID)
			throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			this.publicationService.delete(publicationUUID, userInSession);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
}
