package br.com.daily.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.daily.model.transport.TaskDTO;
import br.com.daily.model.transport.UserDTO;
import br.com.daily.service.TaskService;
import br.com.daily.service.UserService;

@RestController
@RequestMapping("/task")
public class TaskController {

	private static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

	private TaskService taskService;

	private UserService userService;

	public TaskController(TaskService taskService, UserService userService) {
		this.taskService = taskService;
		this.userService = userService;
	}

	@GetMapping
	public ResponseEntity<List<TaskDTO>> list(@RequestHeader("token") final String token) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			List<TaskDTO> response = this.taskService.list(userInSession);
			if (response.isEmpty()) {
				return ResponseEntity.noContent().build();
			}
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return ResponseEntity.noContent().build();
		}
	}

}
