package br.com.daily.controllers;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.daily.model.transport.UserDTO;
import br.com.daily.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	private UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@PostMapping
	public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) throws Exception {
		try {
			UserDTO response = this.userService.createUser(userDTO);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping("/login")
	public ResponseEntity<String> login(HttpServletRequest request, @RequestBody UserDTO userDTO) throws Exception {
		try {
			String userRemoteAddress = request.getRemoteAddr();
			userDTO.setRemoteAddress(userRemoteAddress);
			String token = this.userService.login(userDTO);
			return ResponseEntity.ok(token);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping("/logout")
	public ResponseEntity<String> logout(@RequestHeader("token") String token) throws Exception {
		try {
			this.userService.logout(token);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

}
