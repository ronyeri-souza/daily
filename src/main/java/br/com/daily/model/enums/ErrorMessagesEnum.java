package br.com.daily.model.enums;

public enum ErrorMessagesEnum {

	BLOCKED_IP("Unable to log in as you have reached the maximum number of attempts. Try again in 30 minutes"),
	INVALID_PASSWORD("A valid password must be provided to login."),
	INVALID_PASSWORD_REGEX(
			"The password must contain at least 8 characters, with at least one capital letter, one number and one special character."),
	INVALID_PERMISSION_PROJECT_GENERAL("Only the project owner can perform this operation"),
	INVALID_PERMISSION_PROJECT_REMOVE_USER("Only the project owner can delete other project users"),
	INVALID_TOKEN("There is no user associated with this token: "),
	NON_MEMBERS_CANNOT_CREATE_POSTS("Only project members can create posts"),
	NON_MEMBERS_CANNOT_DELETE_POSTS("Only the post owner or project owner can delete a post"),
	NON_MEMBERS_CANNOT_UPDATE_POSTS("Only the post owner or project owner can edit a post"),
	OWNER_CANNOT_LEAVE_PROJECT("The project owner cannot leave it"),
	PROJECT_BY_ACCESS_TOKEN_NOT_FOUND("Project with the given access token is not found: "),
	PROJECT_BY_IDENTIFIER_NOT_FOUND("Project with the given identifier is not found: "),
	SESSION_TOKEN_EXPIRED("Session expired, you need to login again to generate a valid token"),
	USER_ALREADY_EXISTS("User with the given e-mail already exists: "),
	USER_ALREADY_PROJECT_MEMBER("This user is already a member of the project with name: "),
	USER_BY_EMAIL_NOT_FOUND("User with the given e-mail is not found: "),
	USER_BY_IDENTIFIER_NOT_FOUND("User with the given identifier is not found: ");

	private String error;

	private ErrorMessagesEnum(String error) {
		this.error = error;
	}

	public String getError() {
		return error;
	}
}
