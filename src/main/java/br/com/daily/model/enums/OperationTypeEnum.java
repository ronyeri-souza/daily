package br.com.daily.model.enums;

public enum OperationTypeEnum {

	CREATE_PROJECT("Create project"),
	CREATE_PROJECT_ACCESS_TOKEN("Create project access token"),
	CREATE_PUBLICATION("Create publication"),
	CREATE_USER("Create user"),
	DELETE_PROJECT("Delete project"),
	DELETE_PUBLICATION("Delete publication"),
	JOIN_PROJECT("Join project"),
	LEAVE_PROJECT("Leave project"),
	LOGIN("Login"),
	UPDATE_PUBLICATION("Update publication");

	private String operation;

	private OperationTypeEnum(String operation) {
		this.operation = operation;
	}

	public String getOperation() {
		return operation;
	}

}
