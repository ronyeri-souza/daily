package br.com.daily.model.enums;

public enum TaskStatusEnum {

	SUCCESS(200, "Success"),
	ERROR(500, "Error");

	private Integer code;
	private String message;

	private TaskStatusEnum(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
