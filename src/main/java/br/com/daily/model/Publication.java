package br.com.daily.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import br.com.daily.model.transport.PublicationDTO;

@Entity
public class Publication {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String dailyIdentifier;

	@Lob
	private String message;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean deleted;

	@Column(nullable = false)
	private LocalDateTime createdAt;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	@OneToOne
	@JoinColumn(name = "project_id")
	private Project project;

	public Publication() {

	}

	public Publication(PublicationDTO publicationDTO) {
		this.setDailyIdentifier(publicationDTO.getDailyIdentifier() != null ? publicationDTO.getDailyIdentifier()
				: UUID.randomUUID().toString());
		this.setMessage(publicationDTO.getMessage());
		this.setDeleted(publicationDTO.getDeleted() != null ? publicationDTO.getDeleted() : false);
		this.setCreatedAt(publicationDTO.getCreatedAt() != null ? publicationDTO.getCreatedAt() : LocalDateTime.now());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDailyIdentifier() {
		return dailyIdentifier;
	}

	public void setDailyIdentifier(String dailyIdentifier) {
		this.dailyIdentifier = dailyIdentifier;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}
