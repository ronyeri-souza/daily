package br.com.daily.model;

import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import br.com.daily.model.transport.ProjectDTO;

@Entity
public class Project {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String dailyIdentifier;

	@Column(nullable = true, unique = true)
	private String accessToken;

	@Column(nullable = false)
	private String name;

	@Lob
	private String description;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean deleted;

	@OneToOne
	@JoinColumn(name = "owner_id")
	private User owner;

	@ManyToMany
	@Cascade(CascadeType.ALL)
	@JoinTable(name = "user_project", joinColumns = { @JoinColumn(name = "project_id") }, inverseJoinColumns = {
			@JoinColumn(name = "user_id") })
	private Set<User> users;

	public Project() {

	}

	public Project(ProjectDTO projectDTO) {
		this.setDailyIdentifier(projectDTO.getDailyIdentifier() != null ? projectDTO.getDailyIdentifier()
				: UUID.randomUUID().toString());
		this.setAccessToken(projectDTO.getAccessToken());
		this.setName(projectDTO.getName());
		this.setDescription(projectDTO.getDescription());
		this.setDeleted(projectDTO.getDeleted() != null ? projectDTO.getDeleted() : false);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDailyIdentifier() {
		return dailyIdentifier;
	}

	public void setDailyIdentifier(String dailyIdentifier) {
		this.dailyIdentifier = dailyIdentifier;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

}
