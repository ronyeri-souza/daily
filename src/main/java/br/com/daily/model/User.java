package br.com.daily.model;

import java.security.NoSuchAlgorithmException;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import br.com.daily.model.transport.UserDTO;
import br.com.daily.utils.CryptographyUtils;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String dailyIdentifier;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false, unique = true)
	private String login;

	@Column(nullable = false, unique = true)
	private String email;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private String salt;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean deleted;

	@ManyToMany
	@Cascade(CascadeType.ALL)
	@JoinTable(name = "user_project", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "project_id") })
	private Set<Project> projects;

	public User() {

	}

	public User(UserDTO userDTO) throws NoSuchAlgorithmException {
		this.setName(userDTO.getName());
		this.setLogin(userDTO.getLogin());
		this.setEmail(userDTO.getEmail());
		this.setDailyIdentifier(
				userDTO.getDailyIdentifier() != null ? userDTO.getDailyIdentifier() : UUID.randomUUID().toString());
		this.setDeleted(userDTO.getDeleted() != null ? userDTO.getDeleted() : false);

		this.setSalt(CryptographyUtils.getInstance().generatePasswordSalt());
		this.setPassword(CryptographyUtils.getInstance().createPasswordWithSalt(userDTO.getPassword(), this.getSalt()));
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDailyIdentifier() {
		return dailyIdentifier;
	}

	public void setDailyIdentifier(String dailyIdentifier) {
		this.dailyIdentifier = dailyIdentifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}
}
