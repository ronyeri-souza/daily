package br.com.daily.model.transport;

import java.io.Serializable;
import java.time.LocalDateTime;

import br.com.daily.model.Publication;

public class PublicationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dailyIdentifier;

	private String message;

	private Boolean deleted;

	private LocalDateTime createdAt;

	private UserDTO user;

	private ProjectDTO project;

	public PublicationDTO() {

	}

	public PublicationDTO(Publication publication) {
		this.setDailyIdentifier(publication.getDailyIdentifier());
		this.setMessage(publication.getMessage());
		this.setDeleted(publication.getDeleted());
		this.setCreatedAt(publication.getCreatedAt());
		if (publication.getUser() != null) {
			this.setUser(new UserDTO(publication.getUser()));
		}

		if (publication.getProject() != null) {
			this.setProject(new ProjectDTO(publication.getProject()));
		}
	}

	public String getDailyIdentifier() {
		return dailyIdentifier;
	}

	public void setDailyIdentifier(String dailyIdentifier) {
		this.dailyIdentifier = dailyIdentifier;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public ProjectDTO getProject() {
		return project;
	}

	public void setProject(ProjectDTO project) {
		this.project = project;
	}

}
