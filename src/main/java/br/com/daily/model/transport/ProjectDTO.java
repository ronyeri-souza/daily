package br.com.daily.model.transport;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import br.com.daily.model.Project;

public class ProjectDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dailyIdentifier;

	private String accessToken;

	private String name;

	private String description;

	private Boolean deleted;

	private UserDTO owner;

	private Set<UserDTO> users;

	public ProjectDTO() {

	}

	public ProjectDTO(Project project) {
		this.setDailyIdentifier(project.getDailyIdentifier());
		this.setAccessToken(project.getAccessToken());
		this.setName(project.getName());
		this.setDescription(project.getDescription());
		this.setDeleted(project.getDeleted());
		this.setOwner(new UserDTO(project.getOwner()));

		if (project.getUsers() != null && !project.getUsers().isEmpty()) {
			Set<UserDTO> users = project.getUsers().stream().map(UserDTO::new).collect(Collectors.toSet());
			this.setUsers(users);
		} else {
			this.setUsers(new HashSet<>());
		}
	}

	public String getDailyIdentifier() {
		return dailyIdentifier;
	}

	public void setDailyIdentifier(String dailyIdentifier) {
		this.dailyIdentifier = dailyIdentifier;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public UserDTO getOwner() {
		return owner;
	}

	public void setOwner(UserDTO owner) {
		this.owner = owner;
	}

	public Set<UserDTO> getUsers() {
		return users;
	}

	public void setUsers(Set<UserDTO> users) {
		this.users = users;
	}

}
