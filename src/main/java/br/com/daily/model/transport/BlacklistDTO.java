package br.com.daily.model.transport;

import java.io.Serializable;

public class BlacklistDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dailyIdentifier;

	private String ipAddress;

	private Integer attempts;

	public BlacklistDTO() {

	}

	public BlacklistDTO(String ipAddress, Integer attempts) {
		this.ipAddress = ipAddress;
		this.attempts = attempts;
	}

	public String getDailyIdentifier() {
		return dailyIdentifier;
	}

	public void setDailyIdentifier(String dailyIdentifier) {
		this.dailyIdentifier = dailyIdentifier;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getAttempts() {
		return attempts;
	}

	public void setAttempts(Integer attempts) {
		this.attempts = attempts;
	}

}
