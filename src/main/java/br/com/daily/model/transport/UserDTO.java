package br.com.daily.model.transport;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.daily.model.User;

public class UserDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dailyIdentifier;

	private String name;

	private String login;

	private String email;

	private String password;

	private Boolean deleted;

	@JsonIgnore
	private String remoteAddress;

	private Set<ProjectDTO> projects;

	public UserDTO() {

	}

	public UserDTO(User user) {
		this.setDailyIdentifier(user.getDailyIdentifier());
		this.setEmail(user.getEmail());
		this.setName(user.getName());
		this.setLogin(user.getLogin());
		this.setDeleted(user.getDeleted());
		this.setProjects(new HashSet<>());
	}

	public void clearPassword() {
		this.setPassword(null);
	}

	public String getDailyIdentifier() {
		return dailyIdentifier;
	}

	public void setDailyIdentifier(String dailyIdentifier) {
		this.dailyIdentifier = dailyIdentifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	public Set<ProjectDTO> getProjects() {
		return projects;
	}

	public void setProjects(Set<ProjectDTO> projects) {
		this.projects = projects;
	}
}
