package br.com.daily.model.transport;

import java.io.Serializable;
import java.time.LocalDateTime;

import br.com.daily.model.Task;
import br.com.daily.model.enums.OperationTypeEnum;
import br.com.daily.model.enums.TaskStatusEnum;

public class TaskDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String dailyIdentifier;

	private OperationTypeEnum operation;

	private TaskStatusEnum status;

	private String message;

	private String detail;

	private Boolean deleted;

	private LocalDateTime createdAt;

	private UserDTO user;

	public TaskDTO() {

	}

	public TaskDTO(OperationTypeEnum operation, String detail, UserDTO user, TaskStatusEnum status) {
		this.operation = operation;
		this.detail = detail;
		this.user = user;
		this.status = status;
	}

	public TaskDTO(Task task) {
		this.dailyIdentifier = task.getDailyIdentifier();
		this.operation = task.getOperation();
		this.status = task.getStatus();
		this.message = task.getMessage();
		this.detail = task.getDetail();
		this.deleted = task.getDeleted();
		this.createdAt = task.getCreatedAt();
		if (task.getUser() != null) {
			this.user = new UserDTO(task.getUser());
		}

	}

	public String getDailyIdentifier() {
		return dailyIdentifier;
	}

	public void setDailyIdentifier(String dailyIdentifier) {
		this.dailyIdentifier = dailyIdentifier;
	}

	public OperationTypeEnum getOperation() {
		return operation;
	}

	public void setOperation(OperationTypeEnum operation) {
		this.operation = operation;
	}

	public TaskStatusEnum getStatus() {
		return status;
	}

	public void setStatus(TaskStatusEnum status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}
}
