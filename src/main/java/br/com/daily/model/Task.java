package br.com.daily.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import br.com.daily.model.enums.OperationTypeEnum;
import br.com.daily.model.enums.TaskStatusEnum;
import br.com.daily.model.transport.TaskDTO;

@Entity
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String dailyIdentifier;

	@Enumerated(EnumType.STRING)
	private OperationTypeEnum operation;

	@Enumerated(EnumType.STRING)
	private TaskStatusEnum status;

	@Lob
	@Column(nullable = true)
	private String message;

	@Column(nullable = false)
	private String detail;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean deleted;

	@Column(nullable = false)
	private LocalDateTime createdAt;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Task() {

	}

	public Task(TaskDTO taskDTO) {
		this.setDailyIdentifier(
				taskDTO.getDailyIdentifier() != null ? taskDTO.getDailyIdentifier() : UUID.randomUUID().toString());
		this.setOperation(taskDTO.getOperation());
		this.setStatus(taskDTO.getStatus());
		this.setMessage(taskDTO.getMessage());
		this.setDetail(taskDTO.getDetail());
		this.setDeleted(taskDTO.getDeleted() != null ? taskDTO.getDeleted() : false);
		this.setCreatedAt(taskDTO.getCreatedAt() != null ? taskDTO.getCreatedAt() : LocalDateTime.now());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDailyIdentifier() {
		return dailyIdentifier;
	}

	public void setDailyIdentifier(String dailyIdentifier) {
		this.dailyIdentifier = dailyIdentifier;
	}

	public OperationTypeEnum getOperation() {
		return operation;
	}

	public void setOperation(OperationTypeEnum operation) {
		this.operation = operation;
	}

	public TaskStatusEnum getStatus() {
		return status;
	}

	public void setStatus(TaskStatusEnum status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
