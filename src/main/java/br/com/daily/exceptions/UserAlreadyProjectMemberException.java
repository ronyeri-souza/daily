package br.com.daily.exceptions;

public class UserAlreadyProjectMemberException extends Exception {

	private static final long serialVersionUID = 1L;

	public UserAlreadyProjectMemberException() {

	}

	public UserAlreadyProjectMemberException(String message) {
		super(message);
	}
}
