package br.com.daily.exceptions;

public class BlockedIpException extends Exception {

	private static final long serialVersionUID = 1L;

	public BlockedIpException() {

	}

	public BlockedIpException(String message) {
		super(message);
	}
}
