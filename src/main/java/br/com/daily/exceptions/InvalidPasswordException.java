package br.com.daily.exceptions;

public class InvalidPasswordException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidPasswordException() {

	}

	public InvalidPasswordException(String message) {
		super(message);
	}
}
