package br.com.daily.exceptions;

public class InvalidPermissionException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidPermissionException() {

	}

	public InvalidPermissionException(String message) {
		super(message);
	}
}
