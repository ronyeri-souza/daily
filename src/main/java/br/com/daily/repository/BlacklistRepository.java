package br.com.daily.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.daily.model.Blacklist;

@Repository
public interface BlacklistRepository extends JpaRepository<Blacklist, Long> {

	public Blacklist findByIpAddress(String ipAddress);

	public List<Blacklist> findByBlockedUntilIsNotNull();
}
