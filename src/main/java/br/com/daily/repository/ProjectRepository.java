package br.com.daily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.daily.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

	public Project findByDailyIdentifierAndDeletedFalse(String dailtyIdentifier);

	public Project findByAccessTokenAndDeletedFalse(String accessToken);
}
