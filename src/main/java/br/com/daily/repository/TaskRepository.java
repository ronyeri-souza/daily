package br.com.daily.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.daily.model.Task;
import br.com.daily.model.User;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

	public List<Task> findByUserAndDeletedFalse(User user);
}
