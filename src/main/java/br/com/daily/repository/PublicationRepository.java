package br.com.daily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.daily.model.Publication;

@Repository
public interface PublicationRepository extends JpaRepository<Publication, Long> {

	public Publication findByDailyIdentifierAndDeletedFalse(String dailyIdentifier);
}
