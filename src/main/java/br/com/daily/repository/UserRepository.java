package br.com.daily.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.daily.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	public User findByEmailAndDeletedFalse(String email);

	public User findByLoginAndDeletedFalse(String login);

	public User findByDailyIdentifierAndDeletedFalse(String dailyIdentifier);
}
