package br.com.daily.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.daily.model.UserSession;

@Repository
public interface UserSessionRepository extends JpaRepository<UserSession, Long> {

	@Query("SELECT session FROM UserSession session WHERE deleted = false")
	public List<UserSession> listActiveSessions();

	@Query("SELECT session FROM UserSession session WHERE session.user.dailyIdentifier = :dailyIdentifier AND session.deleted = false")
	public UserSession findByUserUUID(@Param("dailyIdentifier") String userUUID);

	@Query("SELECT session FROM UserSession session WHERE session.token = :token AND session.deleted = false")
	public UserSession findByToken(@Param("token") String token);

	@Modifying
	@Query("UPDATE UserSession SET deleted = true, logoutDate = now() WHERE token = :token")
	public void delete(@Param("token") String token);

}
