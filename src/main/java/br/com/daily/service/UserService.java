package br.com.daily.service;

import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.daily.exceptions.InvalidPasswordException;
import br.com.daily.exceptions.InvalidTokenException;
import br.com.daily.exceptions.UserAlreadyExistsException;
import br.com.daily.exceptions.UserNotFoundException;
import br.com.daily.model.Blacklist;
import br.com.daily.model.User;
import br.com.daily.model.enums.ErrorMessagesEnum;
import br.com.daily.model.enums.OperationTypeEnum;
import br.com.daily.model.enums.TaskStatusEnum;
import br.com.daily.model.transport.TaskDTO;
import br.com.daily.model.transport.UserDTO;
import br.com.daily.model.transport.UserSessionDTO;
import br.com.daily.repository.UserRepository;
import br.com.daily.utils.CryptographyUtils;
import br.com.daily.utils.ValidationUtils;

@Service
public class UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	private UserRepository userRepository;

	private TaskService taskService;

	private UserSessionService userSessionService;

	private BlacklistService blacklistService;

	public UserService(UserRepository userRepository, TaskService taskService, UserSessionService userSessionService,
			BlacklistService blacklistService) {
		this.userRepository = userRepository;
		this.taskService = taskService;
		this.userSessionService = userSessionService;
		this.blacklistService = blacklistService;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public UserDTO getUserByToken(String token) throws Exception {
		try {
			LOGGER.info("Finding user by session token: " + token);
			UserSessionDTO sessionByToken = this.userSessionService.getSessionByToken(token);
			if (sessionByToken == null) {
				throw new InvalidTokenException(ErrorMessagesEnum.INVALID_TOKEN.getError() + token);
			}

			return sessionByToken.getUser();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new InvalidTokenException(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public User findByDailyIdentifier(String dailyIdentifier) throws Exception {
		User user = this.userRepository.findByDailyIdentifierAndDeletedFalse(dailyIdentifier);
		if (user == null) {
			throw new UserNotFoundException(
					ErrorMessagesEnum.USER_BY_IDENTIFIER_NOT_FOUND.getError() + dailyIdentifier);
		}

		return user;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserDTO createUser(UserDTO userDTO) throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.CREATE_USER;
		String detail = "Creating new user with login: " + userDTO.getLogin();
		try {
			LOGGER.info("Starting user creation with login: " + userDTO.getLogin());
			ValidationUtils.getInstance().validatePasswordStrength(userDTO.getPassword());

			User user = this.userRepository.findByEmailAndDeletedFalse(userDTO.getEmail());
			if (user != null) {
				throw new UserAlreadyExistsException(
						ErrorMessagesEnum.USER_ALREADY_EXISTS.getError() + user.getEmail());
			}

			User newUser = new User(userDTO);
			this.userRepository.save(newUser);

			userDTO.setDailyIdentifier(newUser.getDailyIdentifier());
			userDTO.clearPassword();
			if (userDTO.getProjects() == null) {
				userDTO.setProjects(new HashSet<>());
			}

			TaskDTO taskDTO = new TaskDTO(operation, detail, userDTO, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);

			return userDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userDTO, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public String login(UserDTO userDTO) throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.LOGIN;
		String detail = "Logging in with email: " + userDTO.getEmail();
		try {
			User validatedUser = this.validateLoginCredentials(userDTO);
			UserSessionDTO userSession = this.userSessionService.createSession(new UserDTO(validatedUser));

			UserDTO userInSession = new UserDTO(validatedUser);
			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);

			return userSession.getToken();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userDTO, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void logout(String token) {
		this.userSessionService.logoutSession(token);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	private User validateLoginCredentials(UserDTO userDTO) throws Exception {
		try {
			Blacklist blacklist = this.blacklistService.validateLoginAttempt(userDTO.getRemoteAddress());

			User user = this.userRepository.findByEmailAndDeletedFalse(userDTO.getEmail());
			if (user == null) {
				this.blacklistService.save(blacklist);
				throw new UserNotFoundException(
						ErrorMessagesEnum.USER_BY_EMAIL_NOT_FOUND.getError() + userDTO.getEmail());
			}

			if (userDTO.getPassword() == null) {
				this.blacklistService.save(blacklist);
				throw new InvalidPasswordException(ErrorMessagesEnum.INVALID_PASSWORD.getError());
			}

			String passwordToValidate = CryptographyUtils.getInstance().createPasswordWithSalt(userDTO.getPassword(),
					user.getSalt());
			if (!passwordToValidate.equals(user.getPassword())) {
				this.blacklistService.save(blacklist);
				throw new InvalidPasswordException(ErrorMessagesEnum.INVALID_PASSWORD.getError());
			}

			return user;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw e;
		}
	}
}
