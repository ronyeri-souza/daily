package br.com.daily.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.daily.exceptions.BlockedIpException;
import br.com.daily.model.Blacklist;
import br.com.daily.model.enums.ErrorMessagesEnum;
import br.com.daily.repository.BlacklistRepository;

@Service
public class BlacklistService {

	private BlacklistRepository blacklistRepository;

	public BlacklistService(BlacklistRepository blacklistRepository) {
		this.blacklistRepository = blacklistRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Blacklist findByIpAddress(String ipAddress) {
		return this.blacklistRepository.findByIpAddress(ipAddress);
	}

	public Blacklist save(Blacklist blacklist) {
		return this.blacklistRepository.save(blacklist);
	}

	public Blacklist validateLoginAttempt(String ipAddress) throws BlockedIpException {
		Blacklist blacklist = this.findByIpAddress(ipAddress);
		if (blacklist == null) {
			blacklist = new Blacklist(ipAddress, 1);
		} else {

			if (blacklist.getAttempts() == 3) {
				if (blacklist.getBlockedUntil() != null) {
					throw new BlockedIpException(ErrorMessagesEnum.BLOCKED_IP.getError());
				}
				blacklist.setBlockedUntil(LocalDateTime.now().plusMinutes(10));
				this.save(blacklist);
				throw new BlockedIpException(ErrorMessagesEnum.BLOCKED_IP.getError());
			}
			blacklist.setAttempts(blacklist.getAttempts() + 1);
		}

		return blacklist;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void clearExpiredBlacklists() {
		List<Blacklist> blacklists = this.blacklistRepository.findByBlockedUntilIsNotNull();

		if (blacklists != null && !blacklists.isEmpty()) {
			blacklists.stream().forEach(session -> this.isBlockedExpired(session));
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Boolean isBlockedExpired(Blacklist blacklist) {
		if (LocalDateTime.now().isBefore(blacklist.getBlockedUntil())) {
			return false;
		}
		this.blacklistRepository.delete(blacklist);
		return true;
	}
}
