package br.com.daily.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.daily.model.Task;
import br.com.daily.model.User;
import br.com.daily.model.transport.TaskDTO;
import br.com.daily.model.transport.UserDTO;
import br.com.daily.repository.TaskRepository;
import br.com.daily.repository.UserRepository;

@Service
public class TaskService {

	private static final Logger LOGGER = LoggerFactory.getLogger(TaskService.class);

	private TaskRepository taskRepository;
	private UserRepository userRepository;

	public TaskService(TaskRepository taskRepository, UserRepository userRepository) {
		this.taskRepository = taskRepository;
		this.userRepository = userRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<TaskDTO> list(UserDTO userInSession) throws Exception {
		try {
			User user = this.userRepository.findByDailyIdentifierAndDeletedFalse(userInSession.getDailyIdentifier());
			List<Task> tasks = this.taskRepository.findByUserAndDeletedFalse(user);

			return tasks.stream().map(TaskDTO::new).collect(Collectors.toList());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return new ArrayList<>();
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void createTask(TaskDTO taskDTO) {
		User user = this.userRepository.findByDailyIdentifierAndDeletedFalse(taskDTO.getUser().getDailyIdentifier());
		Task task = new Task(taskDTO);
		task.setUser(user);
		this.taskRepository.save(task);
	}

}
