package br.com.daily.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.daily.exceptions.InvalidPermissionException;
import br.com.daily.exceptions.ProjectNotFoundException;
import br.com.daily.exceptions.UserAlreadyProjectMemberException;
import br.com.daily.exceptions.UserNotFoundException;
import br.com.daily.model.Project;
import br.com.daily.model.User;
import br.com.daily.model.enums.ErrorMessagesEnum;
import br.com.daily.model.enums.OperationTypeEnum;
import br.com.daily.model.enums.TaskStatusEnum;
import br.com.daily.model.transport.ProjectDTO;
import br.com.daily.model.transport.TaskDTO;
import br.com.daily.model.transport.UserDTO;
import br.com.daily.repository.ProjectRepository;

@Service
public class ProjectService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectService.class);

	private ProjectRepository projectRepository;

	private UserService userService;

	private TaskService taskService;

	public ProjectService(ProjectRepository projectRepository, UserService userService, TaskService taskService) {
		this.projectRepository = projectRepository;
		this.userService = userService;
		this.taskService = taskService;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Project findByDailyIdentifier(String dailyIdentifier) throws Exception {
		Project project = this.projectRepository.findByDailyIdentifierAndDeletedFalse(dailyIdentifier);
		if (project == null) {
			throw new UserNotFoundException(
					ErrorMessagesEnum.PROJECT_BY_IDENTIFIER_NOT_FOUND.getError() + dailyIdentifier);
		}

		return project;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<ProjectDTO> listProjects(UserDTO userInSession) throws Exception {
		try {
			List<ProjectDTO> projectObjects = new ArrayList<>();

			List<Project> projects = this.projectRepository.findAll();
			if (projects == null) {
				return projectObjects;
			}

			for (Project project : projects) {
				Optional<User> optionalUser = project.getUsers().stream()
						.filter(user -> user.getDailyIdentifier().equals(userInSession.getDailyIdentifier()))
						.findFirst();

				if (optionalUser.isPresent()) {
					projectObjects.add(new ProjectDTO(project));
				}
			}

			return projectObjects;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return new ArrayList<>();
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public ProjectDTO create(ProjectDTO projectDTO, UserDTO userInSession) throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.CREATE_PROJECT;
		String detail = "Creating new project with name: " + projectDTO.getName() + " by user with login: "
				+ userInSession.getLogin();
		try {
			LOGGER.info("Starting project creation with name: " + projectDTO.getName());
			User user = this.userService.findByDailyIdentifier(userInSession.getDailyIdentifier());

			Set<User> members = new HashSet<>();
			members.add(user);

			Project newProject = new Project(projectDTO);
			newProject.setOwner(user);

			if (projectDTO.getUsers() != null && !projectDTO.getUsers().isEmpty()) {

				for (UserDTO userDTO : projectDTO.getUsers()) {
					User newMember = this.userService.findByDailyIdentifier(userDTO.getDailyIdentifier());
					members.add(newMember);
				}
			}

			newProject.setUsers(members);
			Project project = this.projectRepository.save(newProject);

			projectDTO.setDailyIdentifier(project.getDailyIdentifier());
			projectDTO.setOwner(userInSession);
			projectDTO.setUsers(members.stream().map(UserDTO::new).collect(Collectors.toSet()));

			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);

			return projectDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(String projectUUID, UserDTO userInSession) throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.DELETE_PROJECT;
		String detail = "Deleting project with UUID: " + projectUUID + " by user with login: "
				+ userInSession.getLogin();
		try {
			Project project = this.projectRepository.findByDailyIdentifierAndDeletedFalse(projectUUID);
			if (project == null) {
				throw new ProjectNotFoundException(
						ErrorMessagesEnum.PROJECT_BY_IDENTIFIER_NOT_FOUND.getError() + projectUUID);
			}

			if (!project.getOwner().getDailyIdentifier().equals(userInSession.getDailyIdentifier())) {
				throw new InvalidPermissionException(ErrorMessagesEnum.INVALID_PERMISSION_PROJECT_GENERAL.getError());
			}

			project.setDeleted(true);
//			TODO Set list of daily to deleted too
			this.projectRepository.save(project);

			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public String generateAccessMemberToken(String projectUUID, UserDTO userInSession) throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.CREATE_PROJECT_ACCESS_TOKEN;
		String detail = "Creating member access token to UUID project: " + projectUUID + " by user with login: "
				+ userInSession.getLogin();
		try {
			Project project = this.projectRepository.findByDailyIdentifierAndDeletedFalse(projectUUID);
			if (project == null) {
				throw new ProjectNotFoundException(
						ErrorMessagesEnum.PROJECT_BY_IDENTIFIER_NOT_FOUND.getError() + projectUUID);
			}

			if (!project.getOwner().getDailyIdentifier().equals(userInSession.getDailyIdentifier())) {
				throw new InvalidPermissionException(ErrorMessagesEnum.INVALID_PERMISSION_PROJECT_GENERAL.getError());
			}

			if (project.getAccessToken() != null) {
				LOGGER.info("Access token for the project found, replacing it with a new one...");
			}

			project.setAccessToken(UUID.randomUUID().toString());
			this.projectRepository.save(project);

			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);

			return project.getAccessToken();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public ProjectDTO addMember(String accessToken, UserDTO userInSession) throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.JOIN_PROJECT;
		String detail = "Joining project with access token: " + accessToken + " with user login: "
				+ userInSession.getLogin();
		try {
			Project project = this.projectRepository.findByAccessTokenAndDeletedFalse(accessToken);
			if (project == null) {
				throw new ProjectNotFoundException(
						ErrorMessagesEnum.PROJECT_BY_ACCESS_TOKEN_NOT_FOUND.getError() + accessToken);
			}

			if (project.getOwner().getDailyIdentifier().equals(userInSession.getDailyIdentifier())) {
				throw new UserAlreadyProjectMemberException(
						ErrorMessagesEnum.USER_ALREADY_PROJECT_MEMBER.getError() + project.getName());
			}

			User newMember = this.userService.findByDailyIdentifier(userInSession.getDailyIdentifier());

			if (project.getUsers() != null && !project.getUsers().isEmpty()) {
				Optional<User> optionalMember = project.getUsers().stream()
						.filter(member -> member.getDailyIdentifier().equals(userInSession.getDailyIdentifier()))
						.findFirst();

				if (optionalMember.isPresent()) {
					throw new UserAlreadyProjectMemberException(
							ErrorMessagesEnum.USER_ALREADY_PROJECT_MEMBER.getError() + project.getName());
				}

				project.getUsers().add(newMember);
				this.projectRepository.save(project);
			} else {
				project.setUsers(new HashSet<>());
				project.getUsers().add(newMember);
				this.projectRepository.save(project);
			}

			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);

			return new ProjectDTO(project);

		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteMember(String userUUID, String projectUUID, UserDTO userInSession) throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.LEAVE_PROJECT;
		String detail = "Removing user from project with UUID: " + projectUUID;
		try {
			Project project = this.projectRepository.findByDailyIdentifierAndDeletedFalse(projectUUID);
			if (project == null) {
				throw new ProjectNotFoundException(
						ErrorMessagesEnum.PROJECT_BY_IDENTIFIER_NOT_FOUND.getError() + projectUUID);
			}

			if (project.getOwner().getDailyIdentifier().equals(userUUID)) {
				throw new UserAlreadyProjectMemberException(ErrorMessagesEnum.OWNER_CANNOT_LEAVE_PROJECT.getError());
			}

			if (project.getOwner().getDailyIdentifier().equals(userInSession.getDailyIdentifier())
					|| userUUID.equals(userInSession.getDailyIdentifier())) {
				if (project.getUsers() != null && !project.getUsers().isEmpty()) {
					project.getUsers().removeIf(member -> member.getDailyIdentifier().equals(userUUID));
					this.projectRepository.save(project);
				}
			} else {
				throw new InvalidPermissionException(
						ErrorMessagesEnum.INVALID_PERMISSION_PROJECT_REMOVE_USER.getError());
			}

			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}
}
