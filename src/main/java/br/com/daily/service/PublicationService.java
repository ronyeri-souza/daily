package br.com.daily.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.daily.exceptions.InvalidPermissionException;
import br.com.daily.model.Project;
import br.com.daily.model.Publication;
import br.com.daily.model.User;
import br.com.daily.model.enums.ErrorMessagesEnum;
import br.com.daily.model.enums.OperationTypeEnum;
import br.com.daily.model.enums.TaskStatusEnum;
import br.com.daily.model.transport.PublicationDTO;
import br.com.daily.model.transport.TaskDTO;
import br.com.daily.model.transport.UserDTO;
import br.com.daily.repository.PublicationRepository;

@Service
public class PublicationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PublicationService.class);

	private PublicationRepository publicationRepository;

	private UserService userService;

	private ProjectService projectService;

	private TaskService taskService;

	public PublicationService(PublicationRepository publicationRepository, UserService userService,
			ProjectService projectService, TaskService taskService) {
		this.publicationRepository = publicationRepository;
		this.userService = userService;
		this.projectService = projectService;
		this.taskService = taskService;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public PublicationDTO create(PublicationDTO publicationDTO, String projectUUID, UserDTO userInSession)
			throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.CREATE_PUBLICATION;
		String detail = "Creating new publication in project: " + projectUUID + " by user with login: "
				+ userInSession.getLogin();
		try {
			LOGGER.info("Starting publication creation...");
			User user = this.userService.findByDailyIdentifier(userInSession.getDailyIdentifier());
			Project project = this.projectService.findByDailyIdentifier(projectUUID);

			if (project.getUsers() == null || project.getUsers().isEmpty()) {
				throw new InvalidPermissionException(ErrorMessagesEnum.NON_MEMBERS_CANNOT_CREATE_POSTS.getError());
			}

			Optional<User> optionalMember = project.getUsers().stream()
					.filter(member -> member.getDailyIdentifier().equals(userInSession.getDailyIdentifier()))
					.findFirst();

			if (!optionalMember.isPresent()) {
				throw new InvalidPermissionException(ErrorMessagesEnum.NON_MEMBERS_CANNOT_CREATE_POSTS.getError());
			}

			Publication newPublication = new Publication(publicationDTO);
			newPublication.setUser(user);
			newPublication.setProject(project);

			Publication publication = this.publicationRepository.save(newPublication);

			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);

			return new PublicationDTO(publication);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public PublicationDTO update(PublicationDTO publicationDTO, String publicationUUID, UserDTO userInSession)
			throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.UPDATE_PUBLICATION;
		String detail = "Updating publication by user with login: " + userInSession.getLogin();
		try {
			LOGGER.info("Starting publication update...");
			Publication publication = this.publicationRepository.findByDailyIdentifierAndDeletedFalse(publicationUUID);

			if (publication.getProject().getUsers() == null || publication.getProject().getUsers().isEmpty()) {
				throw new InvalidPermissionException(ErrorMessagesEnum.NON_MEMBERS_CANNOT_UPDATE_POSTS.getError());
			}

			Optional<User> optionalMember = publication.getProject().getUsers().stream()
					.filter(member -> member.getDailyIdentifier().equals(userInSession.getDailyIdentifier()))
					.findFirst();

			if (!optionalMember.isPresent()) {
				throw new InvalidPermissionException(ErrorMessagesEnum.NON_MEMBERS_CANNOT_UPDATE_POSTS.getError());
			}

			String ownerIdentifier = publication.getProject().getOwner().getDailyIdentifier();
			if (!userInSession.getDailyIdentifier().equals(publication.getUser().getDailyIdentifier())
					&& !userInSession.getDailyIdentifier().equals(ownerIdentifier)) {
				throw new InvalidPermissionException(ErrorMessagesEnum.NON_MEMBERS_CANNOT_UPDATE_POSTS.getError());
			}

			publication.setMessage(publicationDTO.getMessage());
			this.publicationRepository.save(publication);

			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);

			return new PublicationDTO(publication);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(String publicationUUID, UserDTO userInSession) throws Exception {
		OperationTypeEnum operation = OperationTypeEnum.DELETE_PUBLICATION;
		String detail = "Deleting publication with UUID: " + publicationUUID + " by user with login: "
				+ userInSession.getLogin();
		try {
			LOGGER.info("Starting publication update...");
			Publication publication = this.publicationRepository.findByDailyIdentifierAndDeletedFalse(publicationUUID);

			Project project = publication.getProject();

			if (!publication.getUser().getDailyIdentifier().equals(userInSession.getDailyIdentifier())
					&& project.getOwner().getDailyIdentifier().equals(userInSession.getDailyIdentifier())) {
				throw new InvalidPermissionException(ErrorMessagesEnum.NON_MEMBERS_CANNOT_DELETE_POSTS.getError());
			}

			publication.setDeleted(true);
			this.publicationRepository.save(publication);

			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.SUCCESS);
			this.taskService.createTask(taskDTO);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			TaskDTO taskDTO = new TaskDTO(operation, detail, userInSession, TaskStatusEnum.ERROR);
			taskDTO.setMessage(e.getMessage());
			this.taskService.createTask(taskDTO);
			throw new Exception(e.getMessage());
		}
	}
}
