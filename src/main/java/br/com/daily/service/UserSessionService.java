package br.com.daily.service;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.daily.exceptions.InvalidTokenException;
import br.com.daily.model.User;
import br.com.daily.model.UserSession;
import br.com.daily.model.enums.ErrorMessagesEnum;
import br.com.daily.model.transport.UserDTO;
import br.com.daily.model.transport.UserSessionDTO;
import br.com.daily.repository.UserRepository;
import br.com.daily.repository.UserSessionRepository;

@Service
public class UserSessionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserSessionService.class);

	private UserSessionRepository userSessionRepository;

	private UserRepository userRepository;

	public UserSessionService(UserSessionRepository userSessionRepository, UserRepository userRepository) {
		this.userSessionRepository = userSessionRepository;
		this.userRepository = userRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public UserSessionDTO getSessionByToken(String token) throws InvalidTokenException {
		UserSession userSession = this.userSessionRepository.findByToken(token);
		if (userSession == null) {
			throw new InvalidTokenException(ErrorMessagesEnum.SESSION_TOKEN_EXPIRED.getError());
		}

		if (this.isSessionExpired(userSession)) {
			throw new InvalidTokenException(ErrorMessagesEnum.SESSION_TOKEN_EXPIRED.getError());
		}
		return new UserSessionDTO(userSession);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserSessionDTO createSession(UserDTO userDTO) throws Exception {
		UserSession sessionByUserUUID = this.getSessionByUserUUID(userDTO.getDailyIdentifier());
		if (sessionByUserUUID != null) {
			if (!this.isSessionExpired(sessionByUserUUID)) {
				LOGGER.info("The user has an active session, returning it...");
				return new UserSessionDTO(sessionByUserUUID);
			}
		}
		LOGGER.info("The user does not have an active session, creating...");
		User user = this.userRepository.findByDailyIdentifierAndDeletedFalse(userDTO.getDailyIdentifier());

		UserSession userSession = UserSession.build();
		userSession.setUser(user);
		UserSessionDTO userSessionObject = this.createSession(userSession);
		return userSessionObject;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void logoutSession(String token) {
		UserSession session = this.userSessionRepository.findByToken(token);
		if (session != null) {
			session.setLogoutDate(LocalDateTime.now());
			session.setDeleted(true);
			this.userSessionRepository.save(session);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void clearExpiredSessions() {
		List<UserSession> sessions = this.userSessionRepository.listActiveSessions();

		if (sessions != null && !sessions.isEmpty()) {
			sessions.stream().forEach(session -> this.isSessionExpired(session));
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Boolean isSessionExpired(UserSession userSession) {
		if (LocalDateTime.now().isBefore(userSession.getExpirationDate())) {
			return false;
		}
		this.userSessionRepository.delete(userSession.getToken());
		return true;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private UserSessionDTO createSession(UserSession userSession) {
		return new UserSessionDTO(this.userSessionRepository.save(userSession));
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	private UserSession getSessionByUserUUID(String userUUID) {
		return this.userSessionRepository.findByUserUUID(userUUID);

	}

}
